// Based on PhotoMatrix
// https://gitlab.com/satoshiswife/photomatrix/-/blob/master/PhotoMatrix.pde

// user controlled cam
import peasy.*;
PeasyCam cam;

// skip controls the resolution of the cube
int w, h;

// init offsets for noise and depth
float a, xo, yo, zo, newDepth = .0;

PImage img;

// final variables are constants and we need all the resource power we can get
// so lets use constants
final float pi2 = pow(PI, PI);
final String imgname = "green";
final int skip = 10;

void settings() {
  // turn smoothing on
  smooth();
 
  //load our image into a pixel array
  img = loadImage(imgname+".jpg");
  img.loadPixels();

  // set the canvas the same size as the image, and make sure its an even number
  w=img.width;
  h=img.height;
  if (w % 2 == 1) { 
    w++;
  } 
  if (h % 2 == 1) { 
    h++;
  } 
  // use 3D renderer
  size(w, h, P3D);
}


void setup() { 
  // draw from the center
  rectMode(CENTER);
  strokeWeight(1);
 frameRate(24);

  // init user cam
  cam = new PeasyCam(this, 3200);
}

void draw() {
  w=width;
  h=height;
  background(0);


  // amplitude as brightness
  // iterate through the pixel array
  for (int x = 0; x < img.width; x += skip) {
    for (int y = 0; y < img.height; y += skip) {
    // save draw settings
      pushMatrix();

      // get the colour from current pixel
      color c = img.get(x, y);

      // set vector based on pixel brightness
      PVector pos = new PVector(x, y);
      pos.z = brightness(c);

      // move into 3D position and map pos of z according to its brightness
      translate(-(w/2), -(h/2));
      newDepth = map(pos.z, 0, 255, -(width*0.75), width*0.75);
      
      // move to draw the geometry
      translate(pos.x, pos.y, newDepth);

      // animate the fills/no fills based on a cosine function
      if (round(cos(a)) == 0) {
        noFill();
        stroke(c);
      } else {
        noStroke();
        fill(c);
      }
      // this value is what animates our cosine
      a-=0.0005;

      // draw a box/geometry
      drawPixelBox();

      // revert the draw settings/position
      popMatrix();
    }
  }
}

// draw a box
void drawPixelBox() {  
  // turns out if you go higher than ~16000 in a noise value, it dies
  // lets account for that
  if (zo > 15000) {
    zo=0;
  } else {
    zo+=.0002;
  }
  
  // lets animate the size of the boxes based on a noise field
  float n = noise(xo, yo, zo);
  float scale = map(n, 0, 1, 0, pi2+PI);
  
  // draw the box
  box(scale);
}
